package nl.lolmewn.stats.player;

import java.util.Collection;
import org.bukkit.OfflinePlayer;

/**
 *
 * @author Lolmewn
 */
public interface PlayerManager {
    
    public void addPlayer(OfflinePlayer player, StatsPlayer statsPlayer);
    public StatsPlayer findPlayer(String key);
    public StatsPlayer getPlayer(OfflinePlayer player);
    public Collection<StatsPlayer> getPlayers();
    public boolean hasPlayer(OfflinePlayer player);
    public void loadPlayer(OfflinePlayer player);
    public void unloadPlayer(OfflinePlayer player);
    public void unloadPlayer(StatsPlayer player);

}
