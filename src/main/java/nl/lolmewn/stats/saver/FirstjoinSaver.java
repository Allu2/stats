package nl.lolmewn.stats.saver;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import nl.lolmewn.stats.api.Stat;
import nl.lolmewn.stats.api.StatsAPI;
import nl.lolmewn.stats.api.saver.DataSaver;
import nl.lolmewn.stats.player.StatsPlayer;

/**
 *
 * @author Lolmewn
 */
public class FirstjoinSaver extends DataSaver {

    public FirstjoinSaver(StatsAPI api) {
        super(api);
    }

    @Override
    public boolean save(StatsPlayer player, Stat stat, Connection con) throws SQLException {
        String query = "UPDATE " + this.getAPI().getDatabasePrefix() + "players SET firstjoin=? WHERE player_id=?";
        PreparedStatement st = con.prepareStatement(query);
        long time = (long) player.getStatData(stat, "__GLOBAL__", true).getValue(new Object[]{}, true);
        st.setTimestamp(1, new Timestamp(time));
        st.setInt(2, player.getId());
        st.executeUpdate();
        return true;
    }

}
