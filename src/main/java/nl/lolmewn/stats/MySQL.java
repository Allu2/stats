package nl.lolmewn.stats;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import nl.lolmewn.stats.api.mysql.MySQLAttribute;
import nl.lolmewn.stats.api.mysql.MySQLType;
import nl.lolmewn.stats.api.mysql.StatsTable;
import nl.lolmewn.stats.mysql.MySQLLib;
import org.bukkit.command.CommandSender;

public class MySQL {

    private final String prefix; //host, username, password, database, prefix;
    //private int port;
    private boolean fault;
    private final Main plugin;
    private final MySQLLib mysql;

    public MySQL(Main main, String host, int port, String username, String password, String database, String prefix) {
        this.plugin = main;
        this.prefix = prefix;
        this.mysql = new MySQLLib(main.getLogger(), prefix, host, Integer.toString(port), database, username, password);
        Connection con;
        if ((con = this.mysql.open()) != null) {
            try {
                this.usualChecks(con);
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(MySQL.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            this.setFault(true);
        }
    }

    public boolean isFault() {
        return fault;
    }

    private void setFault(boolean fault) {
        this.fault = fault;
    }

    public int executeStatement(String statement) {
        if (isFault()) {
            System.out.println("[Stats] Can't execute statement, something wrong with connection");
            return 0;
        }
        this.plugin.debugQuery("Executing Statement: " + statement);
        try {
            Connection con = this.mysql.getConnection();
            con.setAutoCommit(true);
            Statement state = con.createStatement();
            int re = state.executeUpdate(statement);
            state.close();
            con.close();
            return re;
        } catch (SQLException e) {
            if (e.getMessage().contains("Unknown column 'blockData'")) {
                this.plugin.getLogger().warning("Found faulty blocks table, fixing...");
                this.executeStatement("DROP TABLE " + prefix + "block");
                this.executeStatement("CREATE TABLE IF NOT EXISTS " + this.prefix + "block"
                        + "(counter int PRIMARY KEY NOT NULL AUTO_INCREMENT, "
                        + "player_id INT NOT NULL, "
                        + "blockID int NOT NULL, "
                        + "blockData BLOB NOT NULL, "
                        + "amount int NOT NULL, "
                        + "break boolean NOT NULL)");
                this.plugin.getLogger().warning("Faulty blocks table fixed.");
            } else {
                Logger.getLogger(MySQL.class.getName()).log(Level.SEVERE, null, e);
            }
        }
        return 0;
    }

    public Connection getConnection() {
        Connection con;
        int tries = 0;
        do {
            con = this.mysql.getConnection();
            tries++;
        } while (con == null && tries < 10);
        return con;
    }

    public void exit() {
        this.mysql.exit();
    }

    public boolean usesOldFormat() {
        try {
            Connection con = this.getConnection();
            Statement st = con.createStatement();
            ResultSet exists = st.executeQuery("SHOW TABLES LIKE '" + prefix + "player'");
            if (!exists.next()) {
                return false;
            }
            ResultSet set = st.executeQuery("SELECT * FROM " + prefix + "player LIMIT 1"); //check if it has a player row
            ResultSetMetaData rsmd = set.getMetaData();
            for (int i = 1; i <= rsmd.getColumnCount(); i++) {
                if (rsmd.getColumnName(i).equals("player")) {
                    set.close();
                    st.close();
                    con.close();
                    return true;
                }
            }
            set.close();
            st.close();
            con.close();
            return false;
        } catch (SQLException ex) {
            Logger.getLogger(MySQL.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public void convertOldFormat() {
        plugin.getLogger().info("=========Stats=========");
        plugin.getLogger().info("= Found old database layout, converting...");
        StatsTable playersTable = new StatsTable(prefix + "players", false, plugin.getSettings().createSnapshots());
        playersTable.addColumn("player_id", MySQLType.INTEGER).addAttributes(MySQLAttribute.AUTO_INCREMENT, MySQLAttribute.NOT_NULL, MySQLAttribute.PRIMARY_KEY);
        playersTable.addColumn("UUID", MySQLType.STRING);
        playersTable.addColumn("name", MySQLType.STRING);
        //firstjoin stat will be handled by the validateColumn 
        this.plugin.getStatsTableManager().put(prefix + "players", playersTable);
        try {
            Connection con = this.getConnection();
            Statement st = con.createStatement();
            st.executeUpdate(playersTable.generateCreateTable());
            plugin.getLogger().info("= New players table generated");
            st.execute("ALTER TABLE " + prefix + "players ADD UNIQUE INDEX id (player_id ASC)");
            st.execute("ALTER TABLE " + prefix + "players ADD UNIQUE INDEX name (name ASC)");
            st.execute("ALTER TABLE " + prefix + "players ADD UNIQUE INDEX uuid (uuid ASC)");
            String query = "INSERT INTO " + prefix + "players (player_id, name) SELECT counter,player FROM " + prefix + "player GROUP BY player";
            st.executeUpdate(query);
            plugin.getLogger().info("= All players added to new table");
            String[] tables = new String[]{"player", "move", "block", "kill", "death"};
            for (String table : tables) {
                plugin.getLogger().info("= Starting to convert table " + table + "...");
                if (st.executeQuery("SHOW INDEXES FROM " + prefix + table + " WHERE Key_name='no_duplicates'").next()) {
                    st.execute("DROP INDEX no_duplicates ON " + this.prefix + table);
                }
                if (!hasColumn(con, table, "player_id")) {
                    st.executeUpdate("ALTER TABLE " + prefix + table + " ADD COLUMN player_id INT AFTER counter");
                    st.execute("ALTER TABLE " + prefix + table + " ADD UNIQUE INDEX counter_UNIQUE (counter ASC)");
                    st.executeUpdate("UPDATE " + prefix + table + " SET player_id=(SELECT player_id FROM " + prefix + "players WHERE " + prefix + "players.name=" + prefix + table + ".player)");
                }
                if (hasColumn(con, table, "player")) {
                    st.executeUpdate("ALTER TABLE " + prefix + table + " DROP COLUMN player");
                }
                if (!hasColumn(con, table, "world")) {
                    PreparedStatement pst = con.prepareStatement("ALTER TABLE " + prefix + table + " ADD COLUMN world VARCHAR(255) DEFAULT ? AFTER player_id");
                    pst.setString(1, plugin.getServer().getWorlds().get(0).getName());
                    pst.executeUpdate();
                    pst.close();
                }
            }
            st.close();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(MySQL.class.getName()).log(Level.SEVERE, null, ex);
        }
        plugin.getLogger().info("= Conversion complete! ");
        plugin.getLogger().info("========================");

    }

    public boolean hasColumn(Connection con, String table, String column) throws SQLException {
        Statement st = con.createStatement();
        ResultSet set = st.executeQuery("SELECT * FROM " + prefix + table + " LIMIT 1");
        ResultSetMetaData rsmd = set.getMetaData();
        for (int i = 1; i <= rsmd.getColumnCount(); i++) {
            if (rsmd.getColumnName(i).equals(column)) {
                return true;
            }
        }
        return false;
    }

    public boolean hasTable(Connection con, String table) throws SQLException {
        return con.getMetaData().getTables(null, null, prefix + table, null).next();
    }

    public void theBigAndUltimateDatabaseChecker(CommandSender sender) {
        try {
            Connection con = this.getConnection();
            sender.sendMessage("Checking all tables");
            for (StatsTable table : this.plugin.getStatsTableManager().values()) {
                sender.sendMessage("Checking table " + table.getName());
                table.validateColumns(con);
                sender.sendMessage("Table checked. All clear.");
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(MySQL.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void usualChecks(Connection con) throws SQLException {
        if (hasTable(con, "player") && hasColumn(con, "player", "fishcatch")) {
            Statement st;
            if (hasColumn(con, "player", "fishcatched")) {
                //both exist, throw all on the old and delete it, then convert
                st = con.createStatement();
                st.execute("UPDATE " + prefix + "player SET fishcatch=fishcatch+fishcatched");
                st.execute("ALTER TABLE " + prefix + "player DROP COLUMN fishcatched");
            }
            st = con.createStatement();
            st.execute("ALTER TABLE " + prefix + "player CHANGE fishcatch fishcatched INT");
            st.close();
        }
    }
}
