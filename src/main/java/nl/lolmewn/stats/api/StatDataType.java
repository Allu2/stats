package nl.lolmewn.stats.api;

/**
 *
 * @author Lolmewn
 */
public enum StatDataType {

    FIXED, DYNAMIC, INCREASING

}
