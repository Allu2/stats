package nl.lolmewn.stats.api.saver;

import java.sql.Connection;
import java.sql.SQLException;
import nl.lolmewn.stats.api.Stat;
import nl.lolmewn.stats.api.StatsAPI;
import nl.lolmewn.stats.player.StatsPlayer;

/**
 *
 * @author Lolmewn
 */
public abstract class DataSaver {

    private StatsAPI api;

    public DataSaver(StatsAPI api) {
        this.api = api;
    }

    public StatsAPI getAPI() {
        return api;
    }

    public abstract boolean save(StatsPlayer player, Stat stat, Connection con) throws SQLException;

}
