package nl.lolmewn.stats.api;

import nl.lolmewn.stats.player.StatData;
import nl.lolmewn.stats.player.StatsPlayer;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * @author Lolmewn
 */
public class StatUpdateEvent extends Event implements Cancellable {

    private static final HandlerList handlers = new HandlerList();
    private boolean cancelled = false;

    private final StatData statData;
    private double value;
    private final Object[] vars;

    public StatUpdateEvent(StatData statData, double value, Object[] vars, boolean async) {
        super(async);
        this.statData = statData;
        this.value = value;
        this.vars = vars;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean bln) {
        cancelled = bln;
    }

    public Stat getStat() {
        return statData.getStat();
    }

    public StatData getStatData() {
        return statData;
    }

    /**
     * Gets the value with which it updates the Stat.
     *
     * @return value to update
     */
    public double getUpdateValue() {
        return value;
    }

    /**
     * Gets the current value, before updating.
     *
     * @return the current value of the statData
     */
    public double getCurrentValue() {
        return statData.getValue(vars);
    }

    public double getNewValue() {
        return this.getCurrentValue() + value;
    }

    public Object[] getVars() {
        return vars;
    }

    public void setUpdateValue(int value) {
        this.value = value;
    }

    public StatsPlayer getPlayer() {
        return statData.getStatsPlayer();
    }

}
