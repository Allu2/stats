/*
 *  Copyright 2013 Lolmewn <info@lolmewn.nl>.
 */
package nl.lolmewn.stats;

import java.io.Serializable;

/**
 *
 * @author Lolmewn <info@lolmewn.nl>
 */
public class GlobalDataHolder implements Serializable {

    private static final long serialVersionUID = 13379001L;

    private final String player, stat, table;
    private final Object[] updates;
    private final double updateValue;
    private final boolean incrementing;

    public GlobalDataHolder(String playername, String statName, String table, Object[] updates, double updateValue, boolean incrementing) {
        this.player = playername;
        this.stat = statName;
        this.table = table;
        this.updates = updates;
        this.updateValue = updateValue;
        this.incrementing = incrementing;
    }

    public boolean isIncrementing() {
        return incrementing;
    }

    public String getPlayer() {
        return this.player;
    }

    public String getStat() {
        return stat;
    }

    public String getTable() {
        return table;
    }

    public double getUpdateValue() {
        return this.updateValue;
    }

    public Object[] getParameters() {
        return this.updates;
    }

}
