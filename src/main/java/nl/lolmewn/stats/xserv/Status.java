package nl.lolmewn.stats.xserv;

/**
 * @author Sybren
 */
public enum Status {

    PING("PING"),
    PONG("PONG"),
    HANDSHAKE("Hi"),
    HANDSHAKE_RESPONSE("Hello"),
    SAVED("Saved %s"),
    REQUEST_SAVE("Requesting save of %s"),
    GRANTED("Granted"),
    REJECTED("Rejected"),
    REQUEST_LOAD("Requesting load of %s"),
    LOADED("Loaded %s");

    static Status getStatus(String receive) {
        for (Status stat : values()) {
            if (receive.startsWith(stat.getMessage().split("%")[0])) {
                return stat;
            }
        }
        return null;
    }

    private final String message;

    private Status(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
