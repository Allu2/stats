/*
 *  Copyright 2013 Lolmewn <info@lolmewn.nl>.
 */
package nl.lolmewn.stats.compat;

import com.vexsoftware.votifier.model.VotifierEvent;
import nl.lolmewn.stats.Main;
import nl.lolmewn.stats.player.StatsPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

/**
 *
 * @author Lolmewn <info@lolmewn.nl>
 */
public class VotifierListener implements Listener {

    private final Main plugin;

    public VotifierListener(Main plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void vote(VotifierEvent event) {
        Player player = plugin.getServer().getPlayer(event.getVote().getUsername());
        String world;
        if (this.plugin.getSettings().getDisabledStats().contains("Votes")) {
            return;
        }
        if (player == null) {
            world = plugin.getServer().getWorlds().get(0).getName();
        } else {
            world = player.getWorld().getName();
        }
        StatsPlayer sp = plugin.getAPI().getPlayer(event.getVote().getUsername());
        if (sp != null) {
            sp.getStatData(plugin.getStatTypes().get("Votes"), world, true).addUpdate(new Object[]{}, 1);
        }
    }

}
