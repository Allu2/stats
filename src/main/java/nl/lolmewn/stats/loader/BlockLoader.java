package nl.lolmewn.stats.loader;

import java.sql.ResultSet;
import java.sql.SQLException;
import nl.lolmewn.stats.api.Stat;
import nl.lolmewn.stats.api.StatsAPI;
import nl.lolmewn.stats.api.loader.DataLoader;
import nl.lolmewn.stats.player.StatsPlayer;

/**
 *
 * @author Lolmewn
 */
public class BlockLoader extends DataLoader {

    public BlockLoader(StatsAPI api) {
        super(api);
    }

    @Override
    public boolean load(StatsPlayer player, Stat stat, ResultSet set) throws SQLException {
        if ((stat.getName().equals("Block break") && set.getBoolean("break")) || (stat.getName().equals("Block place") && !set.getBoolean("break"))) {
            player.getStatData(stat, set.getString("world"), true).setCurrentValue(new Object[]{set.getInt("blockID"), set.getByte("blockData"), set.getBoolean("break")}, set.getInt("amount"));
            return true;
        }
        return false;
    }
}
