/*
 *  Copyright 2013 Lolmewn <info@lolmewn.nl>.
 */
package nl.lolmewn.stats.signs;

import java.util.Iterator;
import nl.lolmewn.stats.signs.events.StatsSignUpdateEvent;

/**
 *
 * @author Lolmewn <info@lolmewn.nl>
 */
public class SignUpdaterThread implements Runnable {

    private final SignDataGetter source;

    public SignUpdaterThread(SignDataGetter from) {
        source = from;
    }

    @Override
    public void run() {
        for (Iterator<String> it = source.toUpdate.keySet().iterator(); it.hasNext();) {
            String location = it.next();
            String[] values = source.toUpdate.get(location);
            it.remove();
            StatsSign sign = source.getPlugin().getSignManager().getSignAt(location);
            if (sign == null) {
                source.getPlugin().getLogger().warning("StatsSign not found at " + location + ", cancelling...");
                continue;
            }
            StatsSignUpdateEvent event = new StatsSignUpdateEvent(sign, values);
            source.getPlugin().getServer().getPluginManager().callEvent(event);
            if (event.isCancelled()) {
                return;
            }
            sign.updateSign(values[0], values[1], values[2], values[3]);
        }
    }

}
