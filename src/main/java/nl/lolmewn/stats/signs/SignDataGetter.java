/*
 *  Copyright 2013 Lolmewn <info@lolmewn.nl>.
 */
package nl.lolmewn.stats.signs;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import nl.lolmewn.stats.Main;
import nl.lolmewn.stats.player.StatsPlayer;
import org.bukkit.ChatColor;

/**
 *
 * @author Lolmewn <info@lolmewn.nl>
 */
public class SignDataGetter implements Runnable {

    private final Main plugin;
    protected ConcurrentHashMap<String, String[]> toUpdate = new ConcurrentHashMap<String, String[]>();

    public SignDataGetter(Main plugin) {
        this.plugin = plugin;
    }

    @Override
    public void run() {
        if (plugin.newConfig) {
            return;
        }
        if (plugin.getMySQL() == null || plugin.getMySQL().isFault()) {
            return;
        }
        Connection con = plugin.getMySQL().getConnection();
        for (StatsSign sign : plugin.getSignManager().getAllSigns()) {
            if (sign.getSignType().equals(SignType.RIGHTCLICK)) {
                continue;
            }
            if (sign.getSignType().equals(SignType.PLAYER)) {
                if (sign.isAttachedToStat()) {
                    continue;
                }
                String[] array = new String[4];
                array[0] = ChatColor.BLACK + "[" + ChatColor.YELLOW + "Stats" + ChatColor.BLACK + "]";
                array[1] = sign.getSignLine().substring(0, 1).toUpperCase() + sign.getSignLine().substring(1).toLowerCase();
                array[2] = "by " + sign.getVariable();
                if (sign.getVariable() == null) {
                    array[3] = "no-one? Error!";
                    this.toUpdate.put(sign.getLocationString(), array);
                    return;
                }
                StatsPlayer player = plugin.getPlayerManager().findPlayer(sign.getVariable());
                if (player == null) {
                    continue; //the stats aren't going to get higher anyway
                }
                String statName = sign.getStat().getName();
                if (statName.equals("Block break")) {
                    array[3] = Double.toString(plugin.getAPI().getTotalBlocksBroken(player.getPlayername()));
                } else if (statName.equals("Block place")) {
                    array[3] = Double.toString(plugin.getAPI().getTotalBlocksPlaced(player.getPlayername()));
                } else if (statName.equals("Death")) {
                    array[3] = Double.toString(plugin.getAPI().getTotalDeaths(player.getPlayername()));
                } else if (statName.equals("Kill")) {
                    array[3] = Double.toString(plugin.getAPI().getTotalKills(player.getPlayername()));
                } else if (statName.equals("Playtime")) {
                    int playTimeSeconds = (int) plugin.getAPI().getPlaytime(player.getPlayername());
                    array[3] = String.format("%dd %dh %dm %ds",
                            TimeUnit.SECONDS.toDays(playTimeSeconds),
                            TimeUnit.SECONDS.toHours(playTimeSeconds) - TimeUnit.SECONDS.toDays(playTimeSeconds) * 24,
                            TimeUnit.SECONDS.toMinutes(playTimeSeconds) - TimeUnit.SECONDS.toHours(playTimeSeconds) * 60,
                            TimeUnit.SECONDS.toSeconds(playTimeSeconds) - TimeUnit.SECONDS.toMinutes(playTimeSeconds) * 60);
                } else {
                    double val = player.getGlobalStatData(sign.getStat()).getValueUnsafe();
                    array[3] = Double.toString(val);
                }
                this.toUpdate.put(sign.getLocationString(), array);
            } else {
                String query;
                ResultSet set;
                if (sign.getSignType().equals(SignType.CUSTOM)) {
                    if (sign.getVariable().equals("UNSET") || sign.getSignLine().equals("UNSET")) {
                        continue;
                    }
                    query = sign.getVariable();
                    set = this.getResultSet(con, query, new Object[]{});
                } else {
                    //SignType == TOTAL
                    String statName = sign.getStat().getName();
                    if (statName.equals("Block break")) {
                        query = "SELECT SUM(amount) as result FROM " + plugin.getSettings().getDbPrefix() + "block" + (" WHERE break=1");
                        set = this.getResultSet(con, query, new Object[]{});
                    } else if (statName.equals("Block place")) {
                        query = "SELECT SUM(amount) as result FROM " + plugin.getSettings().getDbPrefix() + "block" + (" WHERE break=0");
                        set = this.getResultSet(con, query, new Object[]{});
                    } else if (statName.equals("Death")) {
                        query = "SELECT SUM(amount) as result FROM " + plugin.getSettings().getDbPrefix() + "death";
                        set = this.getResultSet(con, query, new Object[]{});
                    } else if (statName.equals("Kill")) {
                        query = "SELECT SUM(amount) as result FROM " + plugin.getSettings().getDbPrefix() + "kill";
                        set = this.getResultSet(con, query, new Object[]{});
                    } else if (statName.equals("Playtime")) {
                        query = "SELECT SUM(playtime) as result FROM " + plugin.getSettings().getDbPrefix() + "player";
                        set = this.getResultSet(con, query, new Object[]{});
                    } else if (sign.getStat().getTable().getName().equals(plugin.getSettings().getDbPrefix() + "player")) {
                        query = "SELECT SUM(" + sign.getStat().getName().toLowerCase().replace(" ", "") + ") as result FROM " + plugin.getSettings().getDbPrefix() + "player";
                        set = this.getResultSet(con, query, new Object[]{});
                    } else {
                        sign.updateSign(ChatColor.BLACK + "[" + ChatColor.YELLOW + "Stats" + ChatColor.BLACK + "]",
                                sign.getSignType().toString().substring(0, 1).toUpperCase() + sign.getSignType().toString().substring(1).toLowerCase(),
                                sign.getSignType() == SignType.GLOBAL ? "in total" : sign.getSignLine().length() > 15 ? sign.getSignLine().substring(15) : "",
                                ChatColor.RED + "Error occured");
                        continue;
                    }
                }
                String[] array = new String[4];
                array[0] = ChatColor.BLACK + "[" + ChatColor.YELLOW + "Stats" + ChatColor.BLACK + "]";
                array[1] = sign.getSignType() == SignType.CUSTOM ? sign.getSignLine() : sign.getSignLine().substring(0, 1).toUpperCase() + sign.getSignLine().substring(1).toLowerCase();
                array[2] = sign.getSignType() == SignType.GLOBAL ? "in total" : sign.getSignLine().length() > 15 ? sign.getSignLine().substring(15) : "";
                try {
                    if (set != null && set.next()) {
                        if (set.getObject(1) == null) {
                            array[3] = ChatColor.RED + "Non-existing";
                        } else if (set.getString(1).equalsIgnoreCase("")) {
                            array[3] = "None!";
                        } else {
                            if (sign.getStat().getName().equals("Playtime")) {
                                int playTimeSeconds = set.getInt(1);
                                array[3] = String.format("%dd %dh %dm %ds",
                                        TimeUnit.SECONDS.toDays(playTimeSeconds),
                                        TimeUnit.SECONDS.toHours(playTimeSeconds) - TimeUnit.SECONDS.toDays(playTimeSeconds) * 24,
                                        TimeUnit.SECONDS.toMinutes(playTimeSeconds) - TimeUnit.SECONDS.toHours(playTimeSeconds) * 60,
                                        TimeUnit.SECONDS.toSeconds(playTimeSeconds) - TimeUnit.SECONDS.toMinutes(playTimeSeconds) * 60);
                            } else {
                                array[3] = set.getString(1);
                            }
                        }
                    } else {
                        array[3] = "Error :O";
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(SignDataGetter.class.getName()).log(Level.SEVERE, null, ex);
                    array[3] = "Error :O";
                }
                this.toUpdate.put(sign.getLocationString(), array);
            }
        }
        try {
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(SignDataGetter.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            plugin.getServer().getScheduler().runTaskLater(plugin, new SignUpdaterThread(this), 1);
        }
    }

    protected Main getPlugin() {
        return plugin;
    }

    private ResultSet getResultSet(Connection con, String query, Object[] variables) {
        try {
            PreparedStatement st = con.prepareStatement(query);
            if (variables != null) {
                if (variables.length != 1 || variables[0] != null) {
                    for (int i = 0; i < variables.length; i++) {
                        st.setObject(i + 1, variables[i]);
                    }
                }

            }
            ResultSet set = st.executeQuery();
            return set;
        } catch (SQLException ex) {
            Logger.getLogger(SignDataGetter.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }
}
