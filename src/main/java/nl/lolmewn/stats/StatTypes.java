package nl.lolmewn.stats;

import java.util.HashMap;
import nl.lolmewn.stats.api.Stat;
import nl.lolmewn.stats.api.StatDataType;
import nl.lolmewn.stats.api.loader.DataLoader;
import nl.lolmewn.stats.api.mysql.MySQLType;
import nl.lolmewn.stats.api.mysql.StatTableType;
import nl.lolmewn.stats.api.mysql.StatsTable;
import nl.lolmewn.stats.api.saver.DataSaver;

/**
 * @author Lolmewn
 */
public class StatTypes extends HashMap<String, Stat> {

    private static final long serialVersionUID = 1337L;

    protected StatTypes() {
    }

    public Stat addStat(String statName, StatDataType type, StatTableType tableType, StatsTable table, MySQLType mType, DataLoader dataLoader, DataSaver dataSaver) {
        return this.addStat(statName, new Stat(statName, type, tableType, table, mType, dataLoader, dataSaver));
    }

    public Stat addStat(String name, StatDataType dataType, StatTableType tableType, StatsTable table, MySQLType type, String valueColumn, String[] varColumns) {
        return this.addStat(name, new Stat(name, dataType, tableType, table, type, valueColumn, varColumns));
    }

    public Stat addStat(String name, Stat stat) {
        this.put(name, stat);
        return stat;
    }

    public Stat find(String line) {
        if (line == null) {
            return null;
        }
        for (String stat : this.keySet()) {
            if (line.toLowerCase().equals(stat.toLowerCase())) {
                return get(stat);
            }
        }
        for (String stat : this.keySet()) {
            if (stat.toLowerCase().startsWith(line.toLowerCase())) {
                return get(stat);
            }
        }
        for (String stat : this.keySet()) {
            if (stat.toLowerCase().replace(" ", "").equals(line.toLowerCase())) {
                return get(stat);
            }
        }
        return null;
    }

}
