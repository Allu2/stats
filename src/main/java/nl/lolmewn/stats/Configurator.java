/*
 *  Copyright 2013 Lolmewn <info@lolmewn.nl>.
 */
package nl.lolmewn.stats;

import org.bukkit.ChatColor;
import org.bukkit.conversations.BooleanPrompt;
import org.bukkit.conversations.Conversation;
import org.bukkit.conversations.ConversationContext;
import org.bukkit.conversations.ConversationFactory;
import org.bukkit.conversations.MessagePrompt;
import org.bukkit.conversations.PluginNameConversationPrefix;
import org.bukkit.conversations.Prompt;
import org.bukkit.conversations.StringPrompt;
import org.bukkit.entity.Player;

/**
 *
 * @author Lolmewn <info@lolmewn.nl>
 */
public class Configurator {

    private ConversationFactory factory;
    private Main plugin;
    private final Player p;
    private Conversation c;

    public Configurator(Main m, final Player p) {
        this.plugin = m;
        this.p = p;
        this.factory = new ConversationFactory(m)
                .withModality(true)
                .withEscapeSequence("stop")
                .withPrefix(new PluginNameConversationPrefix(m, " ", ChatColor.GREEN))
                .withFirstPrompt(new ConversationStartPrompt())
                .withLocalEcho(false);
        m.getServer().getScheduler().runTaskLater(plugin, new Runnable() {
            @Override
            public void run() {
                if (p.isOnline()) {
                    c = factory.buildConversation(p);
                    c.begin();
                } else {
                    plugin.beingConfigged = false;
                    factory = null;
                }
            }
        }, 60L);

    }

    private class ConversationStartPrompt extends MessagePrompt {

        @Override
        protected Prompt getNextPrompt(ConversationContext cc) {
            return new inputPrompt("your database host (usually localhost)", "MySQL-Host",
                    new inputPrompt("your database username (usually root)", "MySQL-User",
                            new inputPrompt("your database password", "MySQL-Pass",
                                    new inputPrompt("your database name (usually minecraft)", "MySQL-Database",
                                            new inputPrompt("your database port (usually 3306)", "MySQL-Port",
                                                    new endPrompt())))));
        }

        @Override
        public String getPromptText(ConversationContext cc) {
            return "Welcome to the automatic configuration wizard for Stats."
                    + " This wizard will guide you through the configuration of the plugin."
                    + " You can type the answers to the questions in plain text, no-one will be able to see them.";
        }

    }

    private class inputPrompt extends StringPrompt {

        private final String key;
        private final String value;
        private final Prompt next;

        public inputPrompt(String key, String value, Prompt next) {
            this.key = key;
            this.value = value;
            this.next = next;
        }

        @Override
        public String getPromptText(ConversationContext cc) {
            return "Please type " + key;
        }

        @Override
        public Prompt acceptInput(ConversationContext cc, String string) {
            cc.setSessionData(value, string);
            return next;
        }

    }

    private class endPrompt extends BooleanPrompt {

        @Override
        protected Prompt acceptValidatedInput(ConversationContext cc, boolean bln) {
            cc.setSessionData("update", bln);
            p.sendMessage("Configuration complete. Checking MySQL connection values...");
            if (plugin.setupMySQL(cc)) {
                p.sendMessage("Connection succesful. Saving config values...");
                plugin.saveValues(cc);
                plugin.beingConfigged = false;
                plugin.newConfig = false;
                plugin.configComplete();
            } else {
                return new connectionFailedPrompt();
            }
            return new Finished();
        }

        @Override
        public String getPromptText(ConversationContext cc) {
            return "Final config value: Allow for automatic updating of this plugin?";
        }

    }

    private class connectionFailedPrompt extends MessagePrompt {

        @Override
        protected Prompt getNextPrompt(ConversationContext cc) {
            return new inputPrompt("your database host (usually localhost)", "MySQL-Host",
                    new inputPrompt("your database username (usually root)", "MySQL-User",
                            new inputPrompt("your database password", "MySQL-Pass",
                                    new inputPrompt("your database name (usually minecraft)", "MySQL-Database",
                                            new inputPrompt("your database port (usually 3306)", "MySQL-Port",
                                                    new endPrompt())))));
        }

        @Override
        public String getPromptText(ConversationContext cc) {
            return "Connection to MySQL database failed! Starting over..";
        }

    }

    private class Finished extends MessagePrompt {

        @Override
        protected Prompt getNextPrompt(ConversationContext cc) {
            return Prompt.END_OF_CONVERSATION;
        }

        @Override
        public String getPromptText(ConversationContext cc) {
            return "Configuration finished!";
        }

    }

}
