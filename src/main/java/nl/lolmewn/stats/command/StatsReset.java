package nl.lolmewn.stats.command;

import nl.lolmewn.stats.Main;
import org.bukkit.command.CommandSender;

/**
 *
 * @author Lolmewn
 */
public class StatsReset extends StatsSubCommand {

    private final StatsResetSelf self;
    private final StatsResetOther other;

    public StatsReset(Main main, String perm) {
        super(main, perm);
        this.self = new StatsResetSelf(main, "stats.reset.self");
        this.other = new StatsResetOther(main, "stats.reset.other");
    }

    @Override
    public boolean execute(CommandSender sender, String[] args) {
        if (args.length == 0) {
            return self.onCommand(sender, null, null, args);
        }
        return other.onCommand(sender, null, null, args);
    }

}
