package nl.lolmewn.stats.command;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import nl.lolmewn.stats.Main;
import nl.lolmewn.stats.api.Stat;
import nl.lolmewn.stats.api.StatDataType;
import nl.lolmewn.stats.api.mysql.MySQLType;
import nl.lolmewn.stats.api.mysql.StatTableType;
import nl.lolmewn.stats.api.mysql.StatsTable;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;

/**
 *
 * @author Lolmewn
 */
public class StatsCreate extends StatsSubCommand {

    public StatsCreate(Main main, String perm) {
        super(main, perm);
    }

    @Override
    public boolean execute(CommandSender sender, String[] args) {
        if (args.length == 0) {
            sender.sendMessage("Correct usage: /stats create <statName>");
            return true;
        }
        String statName = args[0];
        if (getPlugin().getAPI().getStat(statName) != null) {
            sender.sendMessage("A stat with this name already exists! Please use a unique name");
            return true;
        }
        final StatsTable playerTable = getPlugin().getStatsTableManager().get(getPlugin().getSettings().getDbPrefix() + "player");
        Stat stat = getPlugin().getAPI().addStat(
                statName,
                StatDataType.DYNAMIC,
                StatTableType.COLUMN,
                playerTable,
                MySQLType.INTEGER,
                statName,
                null);
        playerTable.addStat(stat, "0");
        saveStat(statName);
        getPlugin().getServer().getScheduler().runTaskAsynchronously(getPlugin(), new Runnable() {

            @Override
            public void run() {
                Connection con = getPlugin().getMySQL().getConnection();
                try {
                    playerTable.validateColumns(con);
                } catch (SQLException ex) {
                    Logger.getLogger(StatsCreate.class.getName()).log(Level.SEVERE, null, ex);
                } finally {
                    try {
                        con.close();
                    } catch (SQLException ex) {
                        Logger.getLogger(StatsCreate.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });
        sender.sendMessage("Custom stat created!");
        return true;
    }

    private void saveStat(String statName) {
        File file = new File(getPlugin().getDataFolder(), "stats.yml");
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException ex) {
                Logger.getLogger(StatsCreate.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        YamlConfiguration yaml = YamlConfiguration.loadConfiguration(file);
        List<String> stats = yaml.getStringList("stats");
        stats.add(statName);
        yaml.set("stats", stats);
        try {
            yaml.save(file);
        } catch (IOException ex) {
            Logger.getLogger(StatsCreate.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
