package nl.lolmewn.stats.command;

import java.util.Arrays;
import nl.lolmewn.stats.Main;
import nl.lolmewn.stats.player.StatData;
import nl.lolmewn.stats.player.StatsPlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author Lolmewn
 */
public class StatsDebug extends StatsSubCommand {

    public StatsDebug(Main main, String perm) {
        super(main, perm);
    }

    @Override
    public boolean execute(CommandSender sender, String[] args) {
        if (args.length == 0) {
            sender.sendMessage("Correct usage: /stats debug me|global");
            return true;
        }
        if (args[0].equalsIgnoreCase("me")) {
            if(!(sender instanceof Player)){
                sender.sendMessage("This can only be performed by a player!");
                return true;
            }
            sender.sendMessage("Your data has been print in the console.");
            StatsPlayer p = getPlugin().getPlayerManager().getPlayer((Player)sender);
            getPlugin().getLogger().info("[Debug] hasPlayerRow: " + p.hasPlayerDatabaseRow());
            getPlugin().getLogger().info("[Debug] Stats ------------");
            for (String world : p.getWorlds()) {
                getPlugin().getLogger().info("[Debug]  world: " + world);
                for (StatData statData : p.getStatsForWorld(world)) {
                    getPlugin().getLogger().info("[Debug]    type: " + statData.getStat().getName());
                    getPlugin().getLogger().info("[Debug]      data: ");
                    for (Object[] vars : statData.getAllVariables()) {
                        getPlugin().getLogger().info("[Debug]        vars: " + Arrays.toString(vars) + " UpdateValue: " + statData.getUpdateValue(vars, false));
                        getPlugin().getLogger().info("[Debug]        vars: " + Arrays.toString(vars) + " CurrentValue: " + statData.getValue(vars));

                    }
                }
            }
            return true;
        }
        if (args[0].equalsIgnoreCase("db") || args[0].equalsIgnoreCase("database")) {
            getPlugin().getMySQL().theBigAndUltimateDatabaseChecker(sender);
            return true;
        }
        getPlugin().server = args[0];
        sender.sendMessage("Global server set to " + getPlugin().server);
        return true;
    }

}
