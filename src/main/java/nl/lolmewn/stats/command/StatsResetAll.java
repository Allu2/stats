package nl.lolmewn.stats.command;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import nl.lolmewn.stats.Main;
import nl.lolmewn.stats.api.StatDataType;
import nl.lolmewn.stats.api.mysql.StatsTable;
import nl.lolmewn.stats.player.StatData;
import nl.lolmewn.stats.player.StatsPlayer;
import static org.bukkit.Bukkit.getServer;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author Lolmewn
 */
public class StatsResetAll extends StatsSubCommand {

    private final HashSet<String> confirm = new HashSet<String>();

    public StatsResetAll(Main main, String perm) {
        super(main, perm);
    }

    @Override
    public boolean execute(CommandSender sender, String[] args) {
        if (!this.confirm.contains(sender.getName())) {
            this.confirm.add(sender.getName());
            sender.sendMessage(ChatColor.BLUE + "Please confirm you really want to " + ChatColor.BOLD + "RESET ALL STATS" + ChatColor.BLUE + " by performing the command again");
            final String name = sender.getName();
            getPlugin().getServer().getScheduler().runTaskLater(getPlugin(), new Runnable() {
                @Override
                public void run() {
                    if (confirm.contains(name)) {
                        confirm.remove(name);
                        Player p = getServer().getPlayerExact(name);
                        if (p != null) {
                            p.sendMessage(ChatColor.RED + "Stats reset command expired.");
                        }
                    }
                }
            }, 200L);
            return true;
        }
        for (StatsPlayer player : getPlugin().getPlayerManager().getPlayers()) {
            for (String world : player.getWorlds()) {
                for (StatData statData : player.getStatsForWorld(world)) {
                    if (statData.getStat().getDataType().equals(StatDataType.FIXED)) {
                        continue; //silly 
                    }
                    for (Object[] vars : statData.getAllVariables()) {
                        if (statData.getStat().equals(getPlugin().getStatTypes().get("Lastjoin"))) {
                            statData.setCurrentValue(vars, System.currentTimeMillis());
                        } else {
                            statData.setCurrentValue(vars, 0);
                        }
                        statData.forceUpdate(vars);
                    }
                }
            }
            player.setHasPlayerDatabaseRow(false);
        }
        Connection con = getPlugin().getMySQL().getConnection();
        for (StatsTable table : getPlugin().getStatsTableManager().values()) {
            if(table.getName().equals(getPlugin().getSettings().getDbPrefix() + "players")){
                continue;
            }
            if (!table.hasColumn("player_id")) {
                //not a player table
                continue;
            }
            try {
                PreparedStatement st = con.prepareStatement("DELETE FROM " + table.getName());
                st.executeUpdate();
                st.close();
            } catch (SQLException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        try {
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        sender.sendMessage(ChatColor.GREEN + "All stats have been reset!");
        this.confirm.remove(sender.getName());
        return true;
    }

}
