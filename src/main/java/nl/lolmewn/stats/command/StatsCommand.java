package nl.lolmewn.stats.command;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import nl.lolmewn.stats.Main;
import nl.lolmewn.stats.api.Stat;
import nl.lolmewn.stats.api.StatsPlayerLoadedEvent;
import nl.lolmewn.stats.player.StatData;
import nl.lolmewn.stats.player.StatsPlayer;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

/**
 *
 * @author Lolmewn
 */
public class StatsCommand implements CommandExecutor, Listener {

    private final Main main;
    private final HashMap<String, StatsSubCommand> subs = new HashMap<String, StatsSubCommand>();

    private final HashMap<String, String> awaitingLoad = new HashMap<String, String>();

    public StatsCommand(Main main) {
        this.main = main;
        this.subs.put("add", new StatsAdd(main, "stats.add"));
        this.subs.put("create", new StatsCreate(main, "stats.create"));
        this.subs.put("debug", new StatsDebug(main, "stats.debug"));
        this.subs.put("drop", new StatsDrop(main, "stats.drop"));
        this.subs.put("import", new StatsImport(main, "stats.import"));
        this.subs.put("reload", new StatsReload(main, "stats.reload"));
        this.subs.put("reset", new StatsReset(main, null)); //perms are handled in their own respective classes
        this.subs.put("sendStats", new StatsSendStats(main, "stats.sendStats"));
        this.subs.put("set", new StatsSet(main, "stats.set"));
        this.subs.put("toggle", new StatsToggle(main, "stats.toggle"));
        this.subs.put("world", new StatsWorld(main, null));
        main.getServer().getPluginManager().registerEvents(this, main);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length != 0 && subs.containsKey(args[0])) {
            return this.subs.get(args[0]).onCommand(sender, command, label, Arrays.copyOfRange(args, 1, args.length));
        }
        if (args.length == 0) {
            if (!(sender instanceof Player)) {
                sender.sendMessage("Console cannot perform this command.");
                return true;
            }
            this.sendSomeCoolStats(sender, (Player) sender);
            return true;
        }
        // Perhaps /stats <stat>
        Stat stat = main.getStatTypes().find(args[0]);
        if (stat != null) {
            if (!(sender instanceof Player)) {
                sender.sendMessage("Console cannot perform this command.");
                return true;
            }
            Player player = (Player) sender;
            StatsPlayer sp = main.getAPI().getPlayer(player);
            if (!sp.hasStat(stat)) {
                sender.sendMessage("You do not have this stat yet!");
                return true;
            }

            StatData globalData = sp.getGlobalStatData(stat);
            double globalValue = 0;
            for (Object[] vars : globalData.getAllVariables()) {
                globalValue += globalData.getValue(vars);
            }

            StatData worldData = sp.getStatData(stat, player.getWorld().getName(), false);
            double worldValue = 0;
            if (worldData != null) {
                for (Object[] vars : worldData.getAllVariables()) {
                    worldValue += worldData.getValue(vars);
                }
            }

            sender.sendMessage(ChatColor.LIGHT_PURPLE + stat.getName());
            sender.sendMessage(ChatColor.LIGHT_PURPLE + "Global value: " + ChatColor.GREEN + globalValue);
            sender.sendMessage(ChatColor.LIGHT_PURPLE + "Current world: " + ChatColor.GREEN + worldValue);
            return true;
        }
        if (sender.hasPermission("stats.view.others")) {
            Player find = main.getServer().getPlayer(args[0]);
            if (find == null) {
                sender.sendMessage(ChatColor.RED + "Loading player (if they exist...)");
                this.awaitingLoad.put(sender.getName(), args[0]);
                main.getPlayerManager().loadPlayer(main.getServer().getOfflinePlayer(args[0]));
                return true;
            }
            this.sendSomeCoolStats(sender, find);
            return true;
        } else {
            sender.sendMessage(ChatColor.RED + "Sorry, you do not have permissions to view someone elses stats!");
            return true;
        }
    }

    @EventHandler
    public void onPlayerLoaded(StatsPlayerLoadedEvent event) {
        if (this.awaitingLoad.containsValue(event.getStatsPlayer().getPlayername())) {
            for (String player : this.awaitingLoad.keySet()) {
                Player p = main.getServer().getPlayerExact(player);
                if (p == null) {
                    this.awaitingLoad.remove(player);
                    continue;
                }
                if (this.awaitingLoad.get(player).equalsIgnoreCase(event.getStatsPlayer().getPlayername())) {
                    this.sendSomeCoolStats(p, main.getServer().getOfflinePlayer(event.getStatsPlayer().getPlayername()));
                    this.awaitingLoad.remove(player);
                }
            }
        }
    }

    public void sendSomeCoolStats(CommandSender sender, OfflinePlayer from) {
        StatsPlayer lookup = main.getPlayerManager().getPlayer(from);
        for (String type : main.getSettings().getCommand()) {
            Stat stat = main.getStatTypes().get(type);
            if (stat == null) {
                stat = main.getStatTypes().get((type.substring(0, 1).toUpperCase() + type.substring(1).toLowerCase()).replace("_", " "));
                if (stat == null) {
                    main.getLogger().warning("Wrongly configured, looking for Stat " + type + ", but it doesn't exist.");
                    continue;
                }
            }
            String prettyName = (type.substring(0, 1).toUpperCase() + type.substring(1).toLowerCase()).replace("_", " ");
            if (!lookup.hasStat(stat)) {
                main.debug("Couldn't find StatData for " + type);
                continue;
            }
            StatData statData = lookup.getGlobalStatData(stat);
            if (stat.getName().equals("Move")) {
                double value = 0;
                for (Object[] vars : statData.getAllVariables()) {
                    value += statData.getValue(vars);
                }
                sender.sendMessage(ChatColor.LIGHT_PURPLE + prettyName + ": " + ChatColor.GREEN + value);
            } else if (stat.getName().equals("Lastjoin") || stat.getName().equals("Lastleave")) {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss,SSS", Locale.ENGLISH);
                sender.sendMessage(ChatColor.LIGHT_PURPLE + prettyName + ": " + ChatColor.GREEN + sdf.format(new Date((long) statData.getValue(new Object[]{}))));
            } else if (stat.getName().equals("Playtime")) {
                int playTimeSeconds = (int) statData.getValue(new Object[]{});
                final String playtime = String.format("%d days, %02d hours, %02d mins, %02d secs",
                        TimeUnit.SECONDS.toDays(playTimeSeconds),
                        TimeUnit.SECONDS.toHours(playTimeSeconds) - TimeUnit.SECONDS.toDays(playTimeSeconds) * 24,
                        TimeUnit.SECONDS.toMinutes(playTimeSeconds) - TimeUnit.SECONDS.toHours(playTimeSeconds) * 60,
                        TimeUnit.SECONDS.toSeconds(playTimeSeconds) - TimeUnit.SECONDS.toMinutes(playTimeSeconds) * 60);
                sender.sendMessage(ChatColor.LIGHT_PURPLE + "Playtime: " + ChatColor.GREEN + playtime);
            } else {
                long v = 0;
                for (Object[] vars : statData.getAllVariables()) {
                    v += statData.getValue(vars);
                }
                sender.sendMessage(ChatColor.LIGHT_PURPLE + prettyName + ": " + ChatColor.GREEN + v);
            }
        }
    }

}
