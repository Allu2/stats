package nl.lolmewn.stats.command;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import nl.lolmewn.stats.Main;
import nl.lolmewn.stats.UUIDFetcher;
import nl.lolmewn.stats.player.StatsPlayer;
import org.bukkit.command.CommandSender;

/**
 *
 * @author Lolmewn
 */
public class StatsImport extends StatsSubCommand {

    public StatsImport(Main main, String perm) {
        super(main, perm);
    }

    @Override
    public boolean execute(final CommandSender sender, String[] args) {
        if (args.length == 0) {
            sender.sendMessage("Please specify from which plugin I should import data");
            sender.sendMessage("Please choose from: statsandachievements");
            return true;
        }
        if (args[0].equalsIgnoreCase("statsandachievements")) {
            if (args.length == 1) {
                sender.sendMessage("This plugin appears to be using a prefix for his database. Please specify it using /stats import statsandachievements <prefix>");
                return true;
            }
            sender.sendMessage("Starting convertion (playtime only for now)");
            sender.sendMessage("This will happen in the background. We'll get back to you when it's done.");
            final String prefix = args[1];
            final String world = this.getPlugin().getServer().getWorlds().get(0).getName(); // It doesn't save worlds so we'll dump everything in the default world.
            this.getPlugin().getServer().getScheduler().runTaskAsynchronously(this.getPlugin(), new Runnable() {

                @Override
                public void run() {
                    try {
                        String dbPref = getPlugin().getSettings().getDbPrefix();
                        Connection con = StatsImport.this.getPlugin().getAPI().getConnection();
                        ResultSet set = con.createStatement().executeQuery("SELECT name,PLAYTIME FROM " + prefix + "players");
                        Map<String, Integer> values = new HashMap<String, Integer>();
                        while (set.next()) {
                            values.put(set.getString("name"), set.getInt("PLAYTIME"));
                        }
                        Map<String, UUID> uuids = new UUIDFetcher(new ArrayList<String>(values.keySet()), true).call();
                        for (String name : uuids.keySet()) {
                            UUID uuid = uuids.get(name);
                            if (getPlugin().getServer().getPlayer(uuid) != null) {
                                // Online 
                                StatsPlayer sp = getPlugin().getPlayerManager().getPlayer(getPlugin().getServer().getPlayer(uuid));
                                sp.getStatData(getPlugin().getStatTypes().get("Playtime"), world, true).addUpdate(new Object[]{}, set.getInt("PLAYTIME"));
                            } else {
                                // check if played before
                                PreparedStatement playedCheck = con.prepareStatement("SELECT * FROM " + dbPref + "players WHERE name=?");
                                playedCheck.setString(1, name);
                                ResultSet check = playedCheck.executeQuery();
                                if (!check.next()) {
                                    // has no account, let's make one
                                    PreparedStatement insertPlayer = con.prepareStatement("INSERT INTO " + dbPref + "players (uuid, name) VALUES (?, ?)", Statement.RETURN_GENERATED_KEYS);
                                    insertPlayer.setString(1, uuid.toString());
                                    insertPlayer.setString(2, name);
                                    insertPlayer.executeUpdate();
                                    int id = insertPlayer.getGeneratedKeys().getInt(1);
                                    PreparedStatement insertPlaytime = con.prepareStatement("INSERT INTO " + dbPref + "player (player_id, playtime) VALUES (?, ?)");
                                    insertPlaytime.setInt(1, id);
                                    insertPlaytime.setInt(2, values.get(name));
                                    insertPlaytime.executeUpdate();
                                } else {
                                    PreparedStatement st = con.prepareStatement("UPDATE " + dbPref + "player SET playtime=playtime+? WHERE player_id=? AND world=?");
                                    st.setInt(1, set.getInt("PLAYTIME"));
                                    st.setInt(2, check.getInt("player_id"));
                                    st.setString(3, world);
                                    st.execute();
                                }
                            }
                            // Just update in DB
                        }
                        con.close();
                        sender.sendMessage("All set!");
                    } catch (SQLException ex) {
                        Logger.getLogger(StatsImport.class.getName()).log(Level.SEVERE, null, ex);
                        sender.sendMessage("Something went wrong, please check the logs!");
                    } catch (Exception ex) {
                        Logger.getLogger(StatsImport.class.getName()).log(Level.SEVERE, null, ex);
                        sender.sendMessage("Something went wrong, please check the logs!");
                    }
                }
            });
        }
        return true;
    }

}
